<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include ('lay-out')
	<body style="margin: 0; padding:0;">
		<div id="cars">
			<h1 id="newpaint">Paint Jobs</h1><br>
        <div id="paint-jobs">
    			<h3>Paint Jobs in Progress</h3>
      			<table cellpadding="3"  id="paint-jobstbl">
              <col width="140"><col width="180"><col width="180"><col width="260">
      			<tr><th>Plate No.</th><th>Current Color</th><th>Target Color</th><th>Action</th></tr>
            <tr><td>ASD 234</td><td>Red</td><td>Blue</td><td id="completed">Mark as Completed</td></tr>
            <tr><td>ASD 234</td><td>Red</td><td>Blue</td><td id="completed">Mark as Completed</td></tr>
            <tr><td>ASD 234</td><td>Red</td><td>Blue</td><td id="completed">Mark as Completed</td></tr>
            <tr><td>ASD 234</td><td>Red</td><td>Blue</td><td id="completed">Mark as Completed</td></tr>
            <tr><td>ASD 234</td><td>Red</td><td>Blue</td><td id="completed">Mark as Completed</td></tr>
      			</table>
        </div>
        <div id="performance">
            <table cellpadding="3" id="performancetbl">
              <col width="130"><col width="50">
            <tr><th colspan= "2">SHOP PERFORMACE</th></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>Total Cars Painted</td><td>10</td></tr>
            <tr><td>Breakdown:</td></tr>
            <tr><td>Blue</td><td>4</td></tr>
            <tr><td>Red</td><td>3</td></tr>
            <tr><td>Green</td><td>3</td></tr>
      			</table>
        </div>
        <div id="queue">
          <h3>Paint Queue</h3>
            <table cellpadding="3"  id="queuetbl">
              <col width="134"><col width="180"><col width="450">
      			<tr><th>Plate No.</th><th>Current Color</th><th>Target Color</th></tr>
            <tr><td>ASD 234</td><td>Red</td><td>Blue</td></tr>
      			</table>
        </div>
<br><br><br><br><br>

		</div>
	</body>
</html>
