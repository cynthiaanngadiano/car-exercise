<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Auto Paint Exam</title>
	<script language="Javascript">
		function changePic(id,loc,newimg) {
			document.getElementById(id).src ="C:\Users\DELL\Desktop\AutoPaintExam\resources\assets\images" + newimg + ".jpg";
		}
	</script>
	<link rel="stylesheet" type="text/css" href="{{ url('../assets/css/style.css') }}" />
	</head>
	<body>
		<header>
			<h1>JUAN'S AUTO PAINT<h2>
		</header>
		<nav>
			<ul>
			  <li><a class="active" href="#new">NEW PAINT JOB</a></li>
			  <li><a href="#jobs">PAINT JOBS</a></li>
			</ul>
		</nav>
		<div id="cars">
			<h1 id="newpaint">New Paint Job</h1><br><br>
			<img id="default-car" src="{{ url('../assets/images/Default.jpg') }}" alt="Default Car Color">
			<img id="arrow" src="../assets/images/arrow.jpg" alt="Turn car color into">
			<img id="new-car" src="../assets/images/Default.jpg" alt="New Car Color">
			<br><br>
			<form>
				<h3 id="details">Car Details</h3>
			<table cellpadding="3">
			<tr><td>Plate No.</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="plate" style="font-size: 16px;"></td></tr>
			<tr><td>Current Color</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="default" onChange="changePic('default-car','images',this.value)">
					  <option value="Default" readonly></option>
					  <option value="Red">Red</option>
					  <option value="Green">Green</option>
					  <option value="Blue">Blue</option>
					</select></td></tr>
			<tr><td>Target Color</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="new" onChange="changePic('new-car','images',this.value)">
					  <option value="Default" readonly></option>
					  <option value="Red">Red</option>
					  <option value="Green">Green</option>
					  <option value="Blue">Blue</option>
					</select></td></tr>
			</table>
			<button class="button">Submit</button>
			</form><br><br><br><br><br>

		</div>
	</body>
</html>