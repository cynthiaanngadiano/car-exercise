<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include ('lay-out')
	<body>
		<div id="cars">
			<h1 id="newpaint">New Paint Job</h1><br><br>
			<img id="default-car" src="{{ URL::to('images/Default.jpg') }}" alt="Default Car Color">
			<img id="arrow" src="{{ URL::to('images/arrow.jpg') }}" alt="Turn car color into">
			<img id="new-car" src="{{ URL::to('images/Default.jpg') }}" alt="New Car Color">
			<br><br>
			<form action="/insert" method="post">
				<h3 id="details">Car Details</h3>
				<table id="detailstbl" cellpadding="3">
				<tr><td>Plate No.</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name ="platenum" id="plate" style="font-size: 16px;"></td></tr>
				<tr><td>Current Color</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<select name="currentcolor">
						  <option value="Default" readonly></option>
						  <option value="Red">Red</option>
						  <option value="Green">Green</option>
						  <option value="Blue">Blue</option>
						</select></td></tr>
				<tr><td>Target Color</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<select name="targetcolor">
						  <option value="Default" readonly></option>
						  <option value="Red">Red</option>
						  <option value="Green">Green</option>
						  <option value="Blue">Blue</option>
						</select></td></tr>
				</table>
				<button class="button">Submit</button>
			</form><br><br><br><br><br>

		</div>
	</body>
</html>
