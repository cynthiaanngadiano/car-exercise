<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Auto Paint</title>
	<link href="{!! asset('css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{{URL('js/jquery-3.3.1.js')}}"> </script>
	</head>
	<body style="margin: 0; padding:0;">
	@yield('content')
		<header>
			<h1>JUAN'S AUTO PAINT<h2>
		</header>
		<nav>
			<ul>
			  <li><a href="{{ url('/new-paint-job') }}">NEW PAINT JOB</a></li>
			  <li><a href="{{ url('/paint-jobs') }}">PAINT JOBS</a></li>
			</ul>
		</nav>
	</body>
</html>
